import math
from random import randint, uniform
import sys
import time

import cv2
import numpy as np
import pyautogui as pag

#banking loop - full zoom out, overhead, inbetween corner of southern most copper ores
#mine->bank
# pos1 =(1208,134)
# pos2 =(970, 147)
#       (1017,174)
#       (727, 147)
#       (364, 404)
# rc    (643, 277)
#       (623, 303)
# Rc    (906, 502)
#       (877, 505)
#       (1077,942)
# rc    (963, 460)
#       (915, 489)


x_coord = [
959, 1208,
970,
1017,
727,
364,
643,
623,
906,
877,
1077,
963,
915
]

y_coord = [541, 136,
147,
174,
147,
404,
277,
303,
502,
505,
942,
460,
489]

pag.doubleClick(1208, 134)
for (x, y) in zip(x_coord, y_coord):
    time.sleep(1)
    pag.doubleClick(x, y)
    time.sleep(4)